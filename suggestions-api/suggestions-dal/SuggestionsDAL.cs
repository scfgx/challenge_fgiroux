﻿using suggestions_model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace suggestions_dal
{
    /// < inheritdoc />
    public class SuggestionsDAL : ISuggestionsDAL
    {
        /// < inheritdoc />
        public List<VilleModel> ObtenirSuggestionsNomVille(string q, string lat = "", string @long = "", int page = 1)
        {
            List<VilleModel> res = new List<VilleModel>();

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=C:\\projets\\challenge_fgiroux\\suggestions-api\\suggestions-dal\\App_Data\\Database.mdf;Integrated Security=True;Connect Timeout=30";

                conn.Open();
                SqlCommand command = new SqlCommand("SELECT name, lat, long, country, stateprov FROM Cities WHERE name LIKE @nomVille");
                command.Parameters.AddWithValue("@nomVille", q + "%");
                command.Connection = conn;

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        res.Add(new VilleModel()
                        {
                            Name = reader[0].ToString(),
                            Pays = reader[3].ToString(),
                            EtatProv = reader[4].ToString(),
                            Latitude = reader[1].ToString(),
                            Longitude = reader[2].ToString()
                        });
                    }
                    /*
                    if (res.Count > 0)
                    {
                        res = (from resultat in res.Skip(25 * (page - 1)).Take(25) select resultat).ToList();
                    }
                    */
                }
            }            

            return res;
        }
    }
}