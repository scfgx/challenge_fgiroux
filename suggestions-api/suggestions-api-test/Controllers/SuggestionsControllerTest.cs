﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using AutoFixture;
using AutoFixture.AutoMoq;
using suggestions_api.Services;
using suggestions_api.Controllers;
using suggestions_model;
using System.Linq;
using System.Web.Http.Results;
using System.Collections.Generic;

namespace suggestions_api_test
{
    [TestClass]
    public class SuggestionsControllerTest
    {

        private readonly IFixture fixture;

        public SuggestionsControllerTest()
        {
            fixture = new Fixture().Customize(new AutoMoqCustomization());
        }

        [TestMethod]
        public void ObtenirSuggestions_DevraitAppelerService()
        {
            var suggestionsService = fixture.Freeze<Mock<ISuggestionsService>>();
            var controller = new SuggestionsController(suggestionsService.Object);

            var nomVilleRechercher = fixture.Create<string>();

            controller.obtenirSuggestionsNomVille(nomVilleRechercher);

            suggestionsService.Verify(s => s.ObtenirSuggestionsNomVille(nomVilleRechercher, "", ""), Times.Once);
        }

        [TestMethod]
        public void ObtenirSuggestions_DevraitRetourner400SiParamatreObligatoireEstAbsent()
        {
            var suggestionsService = fixture.Freeze<Mock<ISuggestionsService>>();
            var controller = new SuggestionsController(suggestionsService.Object);            

            var resultat = controller.obtenirSuggestionsNomVille("");

            Assert.IsInstanceOfType(resultat, typeof(BadRequestErrorMessageResult));
        }

        [TestMethod]
        public void ObtenirSuggestions_DevraitRetourner404SiResultatVide()
        {
            var suggestionsService = fixture.Freeze<Mock<ISuggestionsService>>();
            var controller = new SuggestionsController(suggestionsService.Object);

            var resultat = controller.obtenirSuggestionsNomVille("fff");

            Assert.IsInstanceOfType(resultat, typeof(NotFoundResult));
        }

        [TestMethod]
        public void ObtenirSuggestions_DevraitRetourner200ContenantListeDesSuggestionSiResultatsTrouvees()
        {
            var suggestionsService = fixture.Freeze<Mock<ISuggestionsService>>();
            var controller = new SuggestionsController(suggestionsService.Object);
            var resultatAttendu = fixture.CreateMany<SuggestionModel>().ToList();

            suggestionsService.Setup(s => s.ObtenirSuggestionsNomVille("fff", "", "")).Returns(resultatAttendu);

            var resultat = controller.obtenirSuggestionsNomVille("fff");
            
            Assert.AreEqual(resultatAttendu, ((OkNegotiatedContentResult<List<SuggestionModel>>)resultat).Content);
        }
    }
}
