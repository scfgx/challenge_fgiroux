﻿using AutoFixture;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using suggestions_model;
using System.Linq;
using suggestions_api.Convertisseurs;
using FluentAssertions;
using System.Collections.Generic;

namespace suggestions_api_test.Convertisseurs
{

    [TestClass]
    public class SuggestionConvertisseurTest
    {
        private readonly IFixture fixture;

        public SuggestionConvertisseurTest()
        {
            fixture = new Fixture();
        }

        [TestMethod]
        public void ObtenirSuggestions_DevraitObtenirListeVideSiFourniNull()
        {            
            var convertisseur = new SuggestionConvertisseur();

            var resultat = convertisseur.ConvertirEnSuggestion(null);

            resultat.Count.Should().Be(0);
        }

        [TestMethod]
        public void ObtenirSuggestions_DevraitObtenirListeVideSiFourniListeVide()
        {            
            var convertisseur = new SuggestionConvertisseur();

            var resultat = convertisseur.ConvertirEnSuggestion(new List<VilleModel>());

            resultat.Count.Should().Be(0);
        }

        [TestMethod]
        public void ObtenirSuggestions_DevraitConvertirVille()
        {
            var listeAConvertir = fixture.CreateMany<VilleModel>().ToList();           
            var convertisseur = new SuggestionConvertisseur();

            var resultat = convertisseur.ConvertirEnSuggestion(listeAConvertir);

            resultat.Count.Should().Be(listeAConvertir.Count);
            
            resultat[0].Latitude.Should().Be(listeAConvertir[0].Latitude);
            resultat[0].Longitude.Should().Be(listeAConvertir[0].Longitude);
            resultat[0].Name.Split(',')[0].Should().Be(listeAConvertir[0].Name);
            resultat[0].Name.Split(',')[1].Trim().Should().Be(listeAConvertir[0].EtatProv.ToUpper());
            resultat[0].Name.Split(',')[2].Trim().Should().Be(listeAConvertir[0].Pays);
            resultat[0].Score.Should().Be(0.0);                        
        }

        [TestMethod]
        public void ObtenirSuggestions_DevraitConvertirVilleCanadienne()
        {
            var listeAConvertir = fixture.CreateMany<VilleModel>().ToList();
            listeAConvertir[0].Pays = "CA";
            var convertisseur = new SuggestionConvertisseur();

            var resultat = convertisseur.ConvertirEnSuggestion(listeAConvertir);
           
            resultat[0].Name.Split(',')[2].Trim().Should().Be("Canada");           
        }

        [TestMethod]
        public void ObtenirSuggestions_DevraitConvertirVilleAmericaine()
        {
            var listeAConvertir = fixture.CreateMany<VilleModel>().ToList();
            listeAConvertir[0].Pays = "US";
            var convertisseur = new SuggestionConvertisseur();

            var resultat = convertisseur.ConvertirEnSuggestion(listeAConvertir);

            resultat[0].Name.Split(',')[2].Trim().Should().Be("USA");
        }
    }
}
