﻿using suggestions_api.Convertisseurs;
using suggestions_model;
using suggestions_dal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Configuration;

namespace suggestions_api.Services
{
    /// <inheritdoc />
    public class SuggestionsService : ISuggestionsService
    {

        private readonly ISuggestionConvertisseur _convertisseur;
        private readonly ISuggestionsDAL _dal;

        /// <summary>
        /// Constructeur d'injection des dépendances
        /// </summary>
        /// <param name="dal">La couche d'accès aux données</param>
        /// <param name="convertisseur">Le convertisseur de résultats</param>
        public SuggestionsService(ISuggestionsDAL dal, ISuggestionConvertisseur convertisseur)
        {
            _dal = dal;
            _convertisseur = convertisseur;
        }

        /// <inheritdoc />
        public List<SuggestionModel> ObtenirSuggestionsNomVille(string nomVilleRecherche, string lat = "", string @long = "", int page = 1)
        {
            List<VilleModel> suggestionsObtenues;           

            // Vérifier si resultat deja en cache
            var resCache = (List<VilleModel>)Cache.GetValue(nomVilleRecherche);
            if (resCache != null && resCache.Count > 0)
            {
                suggestionsObtenues = resCache;
            } else
            {
                // Obtenir les suggestions de la BD            
                suggestionsObtenues = _dal.ObtenirSuggestionsNomVille(nomVilleRecherche, lat, @long, page);
                var cacheDureeHeure = int.Parse(WebConfigurationManager.AppSettings["cacheDureeHeure"]);
                Cache.Add(nomVilleRecherche, suggestionsObtenues, DateTimeOffset.UtcNow.AddHours(cacheDureeHeure));
            }

            suggestionsObtenues = (from resultat in suggestionsObtenues.Skip(25 * (page - 1)).Take(25) select resultat).ToList();


            // Les convertirs            
            var suggestionsVilles = _convertisseur.ConvertirEnSuggestion(suggestionsObtenues);

            // Évaluer leur score individuelle
            EvaluerScores(suggestionsVilles, nomVilleRecherche, lat, @long);           

            // Les retourner en ordre décroissant de score
            if (suggestionsVilles.Count > 0)
            {
                return suggestionsVilles.OrderByDescending(sm => sm.Score).ToList();
            }
            return suggestionsVilles;
        }

        /// <summary>
        /// Évalue le score de chacune des villes trouvées
        /// </summary>
        /// <param name="suggestionsVilles"></param>
        /// <param name="nomVilleRecherche">Le nom de la ville recherché</param>
        /// <param name="lat">La latitude de la ville recherchée</param>
        /// <param name="long">La longitude de la ville recherchée</param>
        private void EvaluerScores(List<SuggestionModel> suggestionsVilles, string nomVilleRecherche, string lat = "", string @long = "")
        {
            double latitudeOrigine;
            double longitudeOrigine;            
            double.TryParse(lat, out latitudeOrigine);
            double.TryParse(@long, out longitudeOrigine);

            suggestionsVilles.ForEach(s => s.Score = EvaluerScore(nomVilleRecherche, s, latitudeOrigine, longitudeOrigine));
        }

        /// <summary>
        /// Évalue le score d'une ville. Le score est plus élevé si la différence du nombre de caractères entre la ville 
        /// recherchée et la ville trouvée est plus petite. Aussi, le score est affecté par la proximité de la ville trouvée par 
        /// rapport à la position géographique obtenue en paramètre (originLat et originLong).
        /// </summary>
        /// <param name="nomVilleRecherche">Le nom de la ville recherché</param>
        /// <param name="suggestion"></param>
        /// <param name="latitudeOrigine">La latitude de la position géographique d'origine</param>
        /// <param name="longitudeOrigine">La longitude de la position géographique d'origine</param>
        /// <returns></returns>
        private double EvaluerScore(string nomVilleRecherche, SuggestionModel suggestion, double latitudeOrigine, double longitudeOrigine)
        {
            double score;

            /* TDODO DELETE */
            //latitudeOrigine = 43.70011;
            //longitudeOrigine = -79.4163;

            // Déterminer d'abord le score selon la différence du nombre de caractères entre la ville recherchée et la ville trouvée.
            score = 1.0 / (suggestion.Name.Split(',')[0].Length - nomVilleRecherche.Length + 2);
            
            // Ajuster ensuite le score selon la proximité de la position géographique de l'origine (si fournie)
            if (latitudeOrigine > 0|| longitudeOrigine > 0)
            {
                double latitudeVilleSuggeree, longitudeVilleSuggeree;
  
                double.TryParse(suggestion.Latitude, out latitudeVilleSuggeree);
                double.TryParse(suggestion.Longitude, out longitudeVilleSuggeree);

                double distance = DistanceEntreDeuxPoints(longitudeOrigine, latitudeOrigine, longitudeVilleSuggeree, latitudeVilleSuggeree);

                var scoreDistance = (1.0 / distance);

                score = score + scoreDistance;
            }
            return score;
        }

        private const double PI = 3.141592653589793;
        private const double RADIAN = 6378.16;

        /// <summary>
        /// Converti les degrés en radians
        /// </summary>
        /// <param name="x">Degré</param>
        /// <returns>L'équivalent en radian</returns>
        private double Radians(double x)
        {
            return x * PI / 180;
        }

        /// <summary>
        /// Calcul la distance entre deux points géographique.
        /// </summary>
        /// <param name="lon1">Longitude du premier point géographique</param>
        /// <param name="lat1">Latitude du premier point géographique</param>
        /// <param name="lon2">Longitude du deuxième point géographique</param>
        /// <param name="lat2">Latitude du deuxième point géographique</param>
        /// <returns></returns>
        private double DistanceEntreDeuxPoints(double lon1, double lat1, double lon2, double lat2)
        {
            double dlon = Radians(lon2 - lon1);
            double dlat = Radians(lat2 - lat1);

            double a = (Math.Sin(dlat / 2) * Math.Sin(dlat / 2)) + Math.Cos(Radians(lat1)) * Math.Cos(Radians(lat2)) * (Math.Sin(dlon / 2) * Math.Sin(dlon / 2));
            double angle = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            return angle * RADIAN;
        }
    }
}