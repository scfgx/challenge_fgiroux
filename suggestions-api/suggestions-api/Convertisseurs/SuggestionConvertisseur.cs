﻿using suggestions_model;
using System.Collections.Generic;

namespace suggestions_api.Convertisseurs
{
    /// <inheritdoc />
    public class SuggestionConvertisseur : ISuggestionConvertisseur
    {

        /// <inheritdoc />
        public List<SuggestionModel> ConvertirEnSuggestion(List<VilleModel> suggestions)
        {
            List<SuggestionModel> resultat = new List<SuggestionModel>();

            if (suggestions != null && suggestions.Count > 0)
            {

                suggestions.ForEach(suggestion => resultat.Add(new SuggestionModel()
                {
                    Name = string.Format("{0}, {1}, {2}",
                                         suggestion.Name,
                                         suggestion.EtatProv.ToUpper(),
                                         FormatterPays(suggestion.Pays)),

                    Latitude = suggestion.Latitude,
                    Longitude = suggestion.Longitude
                }));
            }

            return resultat;
        }

        /// <summary>
        /// Formatte le nom du pays
        /// </summary>
        /// <param name="pays">Le nom du pays non formaté</param>
        /// <returns>Le nom du pays formaté</returns>
        private string FormatterPays(string pays)
        {
            string resultat = pays;
            if (pays.ToUpper() == "CA")
            {
                resultat = "Canada";
            }
            else if (resultat == "US")
            {
                resultat = "USA";
            }
            return resultat;
        }
       
    }
}