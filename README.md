# SPA

La SPA suggestions-ui est la couche présentation de l'application d'obtention de suggestions de nom de ville du challenge Vooban.

* Réalisé en Angular 7.
* Utilisation de Bootstrap pour le visuel.
* Inclus des tests unitaires et end-to-end (Protractor).

## Installation
Vous devez avoir node.js (avec npm) et git d'installé sur votre poste.
Après avoir fait le `git clone`, lancer la commande `npm install` dans le répertoire suggestions-ui 
pour installer toutes les dépendances. À partir de ce moment, vous pouvez exécuter les commandes 
principales (ci-dessous) dans le répertoire \suggestions-ui.

## Commandes principales:

* `npm run test`
  
    lance les tests unitaires (voir les fichiers .specs)


* `npm run e2e`

    lance les tests end-to-end (tests cucumbers protractor du répertoire `\suggestions-ui\e2e\features`). L'API doit préalablement être en marche sur `http://localhost:56581/` (voir plus bas). Après l'exécution des tests, allez consulter le sous-répertoire \suggestions-ui\e2e\.tmp
    Il est possible d'obtenir une erreur de création de répertoire (mkdir) lorsqu'on lance la commande `npm run e2e`. Il suffit de la lancer à nouveau et tout est ok. Je n'ai pas pris le temps d'investiguer.


* `ng serve --open`

    lance l'application. Pour fonctionner, l'API doit préalablement être en marche sur `http://localhost:56581/` (voir plus bas.)
    L'application utilise la géolocalisation du navigateur pour fournir les paramètre `lat` (latitude) et `long` (longitude) à l'API. Il faut donc autoriser l'activation de la fonctionnalité dans le navigateur lorsque demandé. Cela permettra de pouvoir calculer la distance de chaque ville trouvées et d'influencer le score.

## Questionnements et hypothèses

 J'avais des questionnement après la lecture de l'énoncé du challenge. J'ai donc pris des hypothèses:
 
* Puisque l'API est appelé lorsqu'il y a 3 caractères de saisies ou plus, il ne se passe rien lorsqu'on saisie la 1ere et 2eme lettre du nom de la ville.
* Puisque l'API est appelé lorsqu'il y a 3 caractères de saisies ou plus, il ne se passe rien lorsqu'on efface la 2eme et 3eme lettre du nom de la ville.
* La recherche n'est pas sensible à la casse.
* Puisqu'un test specflow fait l'assertion que la ville de New London, qui est dans la base de données, n'est pas trouvée lorsqu'on cherche 'londo' alors cela implique qu'on recherche les villes dont le nom commence par le critère recherché.
* Puisqu'un test specflow fait l'assertion que, par rapport à l'emplacement 43.70011, -79.4163, les villes dont la longueur du nom est plus près de celle du nom recherchés viennent avant celles dont le nom est plus long, mais pourtant plus près géographiquement, cela implique que plus la différence de longueur est petite, voire nulle, entre le nom de la ville recherchée et le nom de la ville trouvée, plus la ville trouvée est priorisée.
* le score des villes est ensuite priorisé selon sa proximité par rapport à un autre point géographique.

## Tests unitaires

  J'ai ajouté des tests unitaires TestBed du service de l'appel à l'API. Je sais que la couverture de test unitaire n'est pas complète (je l'aurait fait si ça allait en prod).

# API

API REST .NET utillisé par la SPA

## Démarrer l'API

* **!!! La première fois, il faud d'abord ajuster le ConnectionString du fichier `\suggestions-api\suggestions-dal\SuggestionsDAL.cs` pour le faire pointer vers le répertoire local contenant `\suggestions-api\suggestions-dal\App_Data\Database.mdf`.**

* Il faut ensuite ouvrir la solution `suggestions_api.sln` dans Visual Studio. Il faut la compiler (il faudra peut-être restaurer les packages nuget) et ensuite démarrer (run) le projet suggestions_api. À ce moment,un serveur IIS Express sera lancé sur http://localhost:56581/ à l'écoute des requêtes.

* Si le port de l'API n'est pas 56581, alors il faudra ajuster le port utilisé par la SPA par celui dans lequel s'exécute l'API. Dans la SPA, ajuster le port contenu dans la variable apiUrl du service `SuggestionsService` dans le fichier `\suggestions-ui\src\app\services\suggestions-service.ts` avant d'exécuter les commandes pour démarrer les tests ou la SPA.

## Contenu de la solution

* Couches séparées (API, Accès aux données, modèles),
* Tests unitaires
* Injection de dépendance avec AutoFac
* Le code contient des commentaires de méthode et de classe

## Hypothèse
Les instructions du challenge précisaient que chaque suggestion retournée par l'API doit contenir les informations suivantes: 1) Nom de la ville, son pays et la province ou l'état (Ex: "Montréal, QC, Canada") 2) Latitude 3) Longitude 4) Un score de 0 à 1 indiquant la confiance de la suggestion.
J'ai conclu que c'étais la responabilité de l'API de formater les champs, même si c'est normalement le travail de la couche de présentation.
