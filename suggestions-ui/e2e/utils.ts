import { browser } from 'protractor';
import * as fs from 'fs';

export class Utils {

  navigateTo(url: string) {
    return browser.get(url);
  }

  writeScreenShot(data, filename) {
    const stream = fs.createWriteStream('e2e/.tmp/screenshots/' + filename);
    stream.write(new Buffer(data, 'base64'));
    stream.end();
  }

  creerRepertoireScreenshot() {
    const path = 'e2e/.tmp/screenshots';
    if (fs.existsSync(path)) {
      fs.readdirSync(path).forEach(function(file) {
        fs.unlinkSync(path + '/' + file);
      });
      fs.rmdirSync(path);
    }
    try {
      fs.mkdirSync(path);
    }
    finally {

    }
    return;
  }

}
