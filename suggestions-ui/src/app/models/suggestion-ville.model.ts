export class SuggestionVille {
    Name: string;
    Latitude: number;
    Longitude: number;
    Score: number;

    constructor(city: SuggestionVille) {
        this.Name = city.Name;
        this.Latitude = city.Latitude;
        this.Longitude = city.Longitude;
        this.Score = city.Score;
    }
}
