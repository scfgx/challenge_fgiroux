﻿using suggestions_model;
using System.Collections.Generic;

namespace suggestions_api.Services
{
    /// <summary>
    /// Classe qui offre les services requis a l'obtention de suggestion de noms de villes
    /// </summary>
    public interface ISuggestionsService
    {
        /// <summary>
        /// Obtient les suggestions de noms de villes
        /// </summary>
        /// <param name="q">Le nom de la ville recherché</param>
        /// <param name="lat">La latitude de la ville recherchée</param>
        /// <param name="long">La longitude de la ville recherchée</param>
        /// <returns></returns>
        List<SuggestionModel> ObtenirSuggestionsNomVille(string q, string lat = "", string @long = "", int page = 1);
    }
}
