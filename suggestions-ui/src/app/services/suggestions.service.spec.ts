import { TestBed, getTestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { SuggestionsService } from './suggestions.service';

describe('SuggestionService: getSuggestions ', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [SuggestionsService],
            imports: [
                HttpClientTestingModule
            ],
        });
    });

    afterEach(inject([HttpTestingController], (httpMock: HttpTestingController) => {
        httpMock.verify();
    }));

    it('devrait appeler le bon API via un GET',
        inject([HttpTestingController, SuggestionsService],
            (httpMock: HttpTestingController, service: SuggestionsService) => {
            service.getSuggestions({nomVille: 'ddd', latitude: null, longitude: null}).subscribe();
            const req = httpMock.expectOne('http://localhost:56581/suggestions?q=ddd');
            expect(req.request.method).toEqual('GET');
        })
    );

    it('devrait inclure la latitude optionnelle lorsque présente et ne pas inclure la longitude lorsque absente',
        inject([HttpTestingController, SuggestionsService],
            (httpMock: HttpTestingController, service: SuggestionsService) => {
            service.getSuggestions({nomVille: 'ddd', latitude: '12', longitude: null}).subscribe();
            httpMock.expectOne('http://localhost:56581/suggestions?q=ddd&lat=12');
        })
    );

    it('devrait exclure la latitude optionnelle lorsque absente et inclure la longitude lorsque présente',
        inject([HttpTestingController, SuggestionsService],
            (httpMock: HttpTestingController, service: SuggestionsService) => {
            service.getSuggestions({nomVille: 'ddd', latitude: null, longitude: '34'}).subscribe();
            httpMock.expectOne('http://localhost:56581/suggestions?q=ddd&long=34');
        })
    );

    it('devrait inclure la latitude optionnelle lorsque présente et inclure la longitude lorsque présente',
        inject([HttpTestingController, SuggestionsService],
            (httpMock: HttpTestingController, service: SuggestionsService) => {
            service.getSuggestions({nomVille: 'ddd', latitude: '23', longitude: '34'}).subscribe();
            httpMock.expectOne('http://localhost:56581/suggestions?q=ddd&lat=23&long=34');
        })
    );

});
