import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SuggestionVille } from '../models/suggestion-ville.model';
import { RequeteRechercheVille } from '../models/requete-recherche-ville.modele';

@Injectable()
export class SuggestionsService {

    constructor(private http: HttpClient) {}

    apiUrl = 'http://localhost:56581/suggestions?q={cityName}';


    getSuggestions(requete: RequeteRechercheVille): Observable<SuggestionVille[]> {
        return this.http.get<SuggestionVille[]>(this.genererApiUrl(requete));
    }

    private genererApiUrl(requete: RequeteRechercheVille): string {
        let url = '';
        url = this.apiUrl.replace('{cityName}', requete.nomVille);

        // ajout de la latitude optionnelle
        if (requete.latitude) {
            url = url + '&lat=' + requete.latitude.replace('.', ',');
        }

        // ajout de la longitude optionnelle
        if (requete.longitude) {
            url = url + '&long=' + requete.longitude.replace('.', ',');
        }

        url = url + '&page=' + requete.page.toString();

        return url;

    }

}
