import { Given, When, Then} from 'cucumber';
import { GlobalPage } from './../pages/_global.po';
import { Utils} from './../utils';
import { expect } from 'chai';
import { browser, element, by, $$ } from 'protractor';


When(/^je visite la page de recherche$/, () => {
    return browser.get('http://localhost:4200');
});

When(/^j'entre Amos dans la boite de recherche$/, () => {
    browser.sleep(1000);
    element(by.id('nomVille')).sendKeys('Amos');
    browser.sleep(1000);
});

Then(/^je devrais voir un résultat dans la grille$/, function (dataTable) {
    const lignes = $$('table tbody td');
    lignes.get(0).getText().then(v => expect(v).to.be.equal(dataTable.rawTable[0][0]));
});


When(/^je suis à la position géographique 43.70011, -79.4163$/, () => {

    browser.executeScript(function() {
        window.navigator.geolocation.getCurrentPosition = function(success) {
            success(
                {
                    'coords':
                        {
                            'latitude' : 43.70011,
                            'longitude' : -79.4163,
                            'accuracy': 1,
                            'altitude': 100,
                            'altitudeAccuracy': 1,
                            'heading': 1,
                            'speed': 1
                        },
                    'timestamp' : 1
                }
            );
        };
    });

});

When(/^j'entre Londo dans la boite de recherche$/, () => {
    browser.sleep(1000);
    element(by.id('nomVille')).sendKeys('Londo');
    browser.sleep(3000);
});

Then(/^je devrais voir les résultats suivants dans la grille$/, function (dataTable) {
    const lignes = $$('table tbody td:first-child');
    lignes.get(0).getText().then(v => expect(v).to.be.equal(dataTable.rawTable[0][0]));
    lignes.get(1).getText().then(v => expect(v).to.be.equal(dataTable.rawTable[1][0]));
    lignes.get(2).getText().then(v => expect(v).to.be.equal(dataTable.rawTable[2][0]));
    lignes.get(3).getText().then(v => expect(v).to.be.equal(dataTable.rawTable[3][0]));
});
