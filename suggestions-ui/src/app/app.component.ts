import { Component, OnInit, ViewChild  } from '@angular/core';
import { SuggestionVille } from './models/suggestion-ville.model';
import { SuggestionsService } from './services/suggestions.service';
import { NgZone } from '@angular/core';

declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  suggestionsVille: SuggestionVille[];
  afficherAlerte = false;
  cityName: string;
  page = 1;
  lat: string;
  long: string;

  /*
  @ViewChild('gmap') gmapElement: any;
  map: google.maps.Map;
  */


  _timeout: any = null;

  constructor(private service: SuggestionsService, public lc: NgZone) {}

  ngOnInit() {
    this.initialiserRetourObtentionGeolocation();
  }

  onCityNameChange(cityName: string) {


    this._timeout = null;
    if (this._timeout) {
      window.clearTimeout(this._timeout);
    }
    this._timeout = window.setTimeout(() => {
       this._timeout = null;
       this.lc.run(() => {
        this.cityName = cityName;
        if (cityName.trim().length === 0) {
          this.afficherAlerte = false;
          this.suggestionsVille = [];
        } else {
          if (cityName.length > 2) {
            window.navigator.geolocation.getCurrentPosition(this.getPositionSuccess);
          }
        }
       });
    }, 1000);
/*

    this.cityName = cityName;
    if (cityName.trim().length === 0) {
      this.afficherAlerte = false;
      this.suggestionsVille = [];
    } else {
      if (cityName.length > 2) {
        window.navigator.geolocation.getCurrentPosition(this.getPositionSuccess);
      }
    }
    */
  }

  private initialiserRetourObtentionGeolocation() {
    const conteneurLongitude = $('#geolongitude')[0];
    const observateur = new MutationObserver(() => {
      if (conteneurLongitude && conteneurLongitude.value) {
        this.lat = $('#geolatitude').val();
        this.long = conteneurLongitude.value;
        this.service.getSuggestions(
          {
            nomVille: this.cityName,
            latitude: $('#geolatitude').val(),
            longitude: conteneurLongitude.value,
            page: 1
          }
        ).subscribe(
            suggestionsVille => this.gererSuccesObtentionSuggestion(suggestionsVille),
            erreur => this.gererErreurAppelService(erreur)
        );
      }
    });
    observateur.observe(conteneurLongitude, { attributes: true, attributeFilter: ['value'] });
  }

  private getPositionSuccess(pos) {
    $('#geolatitude').val(pos.coords.latitude);
    $('#geolongitude').val(pos.coords.longitude);
  }

  private gererSuccesObtentionSuggestion(suggestions: SuggestionVille[]) {
    this.afficherAlerte = false;
    this.suggestionsVille = suggestions;
  }

  private gererErreurAppelService(erreur) {
    if (erreur.status === 404) {
      this.suggestionsVille = [];
    } else {
      this.afficherAlerte = true;
    }
  }

  suivant() {
    this.page = this.page + 1;
    this.service.getSuggestions(
      {
        nomVille: this.cityName,
        latitude: this.lat,
        longitude: this.long,
        page: this.page
      }
    ).subscribe(
        suggestionsVille => this.gererSuccesObtentionSuggestion(suggestionsVille),
        erreur => this.gererErreurAppelService(erreur)
    );
  }

  precedent() {
    if (this.page > 1) {
      this.page = this.page - 1;
      this.service.getSuggestions(
        {
          nomVille: this.cityName,
          latitude: this.lat,
          longitude: this.long,
          page: this.page
        }
      ).subscribe(
          suggestionsVille => this.gererSuccesObtentionSuggestion(suggestionsVille),
          erreur => this.gererErreurAppelService(erreur)
      );
    }
  }

}
