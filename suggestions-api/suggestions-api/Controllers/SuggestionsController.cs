﻿using System.Web.Http;
using suggestions_api.Services;

namespace suggestions_api.Controllers
{
    /// <summary>
    /// Controller servant à obtenir des suggestions de nom de villes.
    /// </summary>
    [RoutePrefix("suggestions")]
    public class SuggestionsController : ApiController
    {
        private readonly ISuggestionsService _service;

        /// <summary>
        /// Constructeur d'injection des dépendances
        /// </summary>
        /// <param name="service"></param>
        public SuggestionsController(ISuggestionsService service)
        {
            _service = service;
        }

        /// <summary>
        /// Obtient les suggestions de nom de villes basées sur les critères reçus.
        /// </summary>
        /// <param name="q">Le nom de la ville recherché, obligatoire</param>
        /// <param name="lat">La latitude, optionnelle</param>
        /// <param name="long">La longitude, optionnelle</param>
        /// <returns></returns>
        [HttpGet]
        [Route("")]        
        public IHttpActionResult obtenirSuggestionsNomVille(string q, string lat = "", string @long = "", int page = 1)
        {            
            if (string.IsNullOrEmpty(q))
            {
                return BadRequest("Paramètre q absent");
            }

            var suggestionsObtenues = _service.ObtenirSuggestionsNomVille(q, lat, @long, page);

            if(suggestionsObtenues.Count == 0)
            {
                return NotFound();
            }            
            return Ok(suggestionsObtenues);
        }

    }


}