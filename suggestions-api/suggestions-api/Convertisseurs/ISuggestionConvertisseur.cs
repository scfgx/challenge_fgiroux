﻿using suggestions_model;
using System.Collections.Generic;

namespace suggestions_api.Convertisseurs
{
    /// <summary>
    /// Convertisseur des données relatives aux suggestions de noms de villes
    /// </summary>
    public interface ISuggestionConvertisseur
    {
        /// <summary>
        /// Converti les villes obtenus de la couche d'accès aux données en suggestion
        /// </summary>
        /// <param name="suggestions">La liste des villes reçues de la couche d'accès aux données</param>
        /// <returns>Une suggestion convertie</returns>
        List<SuggestionModel> ConvertirEnSuggestion(List<VilleModel> suggestions);
    }
}
