import {browser, by, element} from 'protractor';
import { Utils } from './../utils';

export class GlobalPage {
    getTitrePrincipal() {
        return element(by.css('h1')).getText();
    }
}
