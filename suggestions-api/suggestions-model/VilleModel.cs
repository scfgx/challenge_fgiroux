﻿namespace suggestions_model
{
    public class VilleModel
    {
        public string Name { get; set; }
        public string Pays { get; set; }
        public string EtatProv { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }        
    }
}