﻿namespace suggestions_model
{
    public class SuggestionModel
    {
        public string Name { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public double Score { get; set; }
    }
}