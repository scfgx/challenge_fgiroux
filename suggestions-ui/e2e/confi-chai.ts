import * as chai from 'chai';
import * as chaiAsPromised from 'chai-as-promised';
import * as cucumber from 'cucumber';

chai.use(chaiAsPromised);
cucumber.setDefaultTimeout(60 * 1000);
