import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, NgModel } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { SuggestionsService } from './services/suggestions.service';
import { AgmCoreModule } from '@agm/core';
import { DebounceModule } from 'ngx-debounce';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    DebounceModule,
    AgmCoreModule.forRoot({
      apiKey: 'YOUR_KEY',
      libraries: ['places']
    })
  ],
  providers: [ SuggestionsService, [NgModel] ],
  bootstrap: [AppComponent]
})
export class AppModule { }
