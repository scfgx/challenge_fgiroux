﻿using suggestions_model;
using System.Collections.Generic;

namespace suggestions_dal
{
    /// <summary>
    /// Interface servant de couches d'accès aux données des villes
    /// </summary>
    public interface ISuggestionsDAL
    {
        /// <summary>
        /// Permet d'obtenir les données de villes de la base de données selon les critères
        /// reçus en paramètre.
        /// </summary>
        /// <param name="q">Le nom de la ville recherché</param>
        /// <param name="lat">La latitude de la ville recherchée</param>
        /// <param name="long">La longitude de la ville recherchée</param>
        /// <returns></returns>
        List<VilleModel> ObtenirSuggestionsNomVille(string q, string lat = "", string @long = "", int page = 1);
    }
}
