export class RequeteRechercheVille {
    nomVille: string;
    latitude: string;
    longitude: string;
    page: number;
}
