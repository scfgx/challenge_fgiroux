import * as AxeBuilder from 'axe-webdriverjs';
import { After, BeforeAll, HookScenarioResult, Status, Then } from 'cucumber';
import { browser } from 'protractor';
import { GlobalPage } from './../pages/_global.po';
import { Utils } from './../utils';

const globalPage: GlobalPage = new GlobalPage();
const utils: Utils = new Utils();

BeforeAll(() => {
  return utils.creerRepertoireScreenshot();
});

After((code: HookScenarioResult) => {
  browser.takeScreenshot().then(function(png) {
    const filename = code.pickle.name + '.png';
    return utils.writeScreenShot(png, filename);
  });

  return browser.executeScript(() => { });

});

Then(/^le site doit être accessible$/, (next) => {
  AxeBuilder(browser)
    .disableRules(['color-contrast'])
    .analyze(function(results) {
      if (results.violations.length > 0) {
        let errors = '';
        for (const violation of results.violations) {
          errors += '\n\tDescription: ' + violation.description + '\n';
          errors += '\tHelp: ' + violation.help + '\n';
          errors += '\tHelpURL: ' + violation.helpUrl + '\n';
          errors += '\tImpact: ' + violation.impact + '\n';
          for (const nodeViolation of violation.nodes) {
            errors += '\n\t\tMessage: ' + nodeViolation.failureSummary + '\n';
            errors += '\t\tHTML: ' + nodeViolation.html + '\n';
          }
        }
        errors += '\nJSONComplet: ' + JSON.stringify(results.violations);
        const exception: Error = new Error(`Erreur dans la validation de l'accessibilité`);
        exception.stack = errors;
        throw exception;
      }
      next();
    });

});
