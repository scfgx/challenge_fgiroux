﻿using System.Reflection;
using System.Web.Http;
using System.Web.Http.Cors;
using Autofac;
using Autofac.Integration.WebApi;
using suggestions_api.Convertisseurs;
using suggestions_api.Services;
using suggestions_dal;

namespace suggestions_api
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {

            var builder = new ContainerBuilder();
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterType<SuggestionConvertisseur>().As<ISuggestionConvertisseur>();
            builder.RegisterType<SuggestionsService>().As<ISuggestionsService>();
            builder.RegisterType<SuggestionsDAL>().As<ISuggestionsDAL>();
            var container = builder.Build();
            var resolver = new AutofacWebApiDependencyResolver(container);
            GlobalConfiguration.Configuration.DependencyResolver = resolver;


            // Configuration et services API Web
            var cors = new EnableCorsAttribute("http://localhost:4200", "*", "*");
            cors.SupportsCredentials = true;
            config.EnableCors(cors);

            // Itinéraires de l'API Web
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
