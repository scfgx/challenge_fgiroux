﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using suggestions_api.Convertisseurs;
using suggestions_api.Services;
using suggestions_dal;
using suggestions_model;
using System.Collections.Generic;
using System.Linq;

namespace suggestions_api_test.Services
{

    [TestClass]
    public class SuggestionsServiceTest
    {
        private readonly IFixture fixture;
        private readonly SuggestionsService service;
        private readonly string q;
        private readonly string latitude;
        private readonly string longitude;
        private readonly Mock<ISuggestionsDAL> mockSuggestionsDAL;
        private readonly Mock<ISuggestionConvertisseur> mockSuggestionConvertisseur;
        private readonly List<VilleModel> resultatDal;

        public SuggestionsServiceTest()
        {
            fixture = new Fixture().Customize(new AutoMoqCustomization());
            q = fixture.Create<string>();
            latitude = fixture.Create<string>();
            longitude = fixture.Create<string>();

            var ville1 = new VilleModel() { Name = "ville1", Pays = "CA", Latitude = "43.123", Longitude = "-79.5432", EtatProv = "QC" };
            var ville2 = new VilleModel() { Name = "ville2", Pays = "US", Latitude = "48.123", Longitude = "-75.5432", EtatProv = "NY" };
            var ville3 = new VilleModel() { Name = "ville3", Pays = "CA", Latitude = "42.456", Longitude = "-78.5689", EtatProv = "ON" };

            resultatDal = new List<VilleModel>() { ville1, ville2, ville3 };
            mockSuggestionsDAL = fixture.Freeze<Mock<ISuggestionsDAL>>();
            mockSuggestionsDAL.Setup(dal => dal.ObtenirSuggestionsNomVille(q, latitude, longitude)).Returns(resultatDal);
            mockSuggestionConvertisseur = fixture.Freeze<Mock<ISuggestionConvertisseur>>();
            service = fixture.Create<SuggestionsService>();
        }
        [TestMethod]
        public void ObtenirSuggestions_DevraitAppelerLaCoucheAccesAuxDonnees()
        {
            service.ObtenirSuggestionsNomVille(q, latitude, longitude);

            mockSuggestionsDAL.Verify(dal => dal.ObtenirSuggestionsNomVille(q, latitude, longitude), Times.Once());
        }

        [TestMethod]
        public void ObtenirSuggestions_DevraitConvertirLeResultatObtenuDeLaCoucheAccesAuxDonnees()
        {
            service.ObtenirSuggestionsNomVille(q, latitude, longitude);

            mockSuggestionConvertisseur.Verify(convertisseur => convertisseur.ConvertirEnSuggestion(resultatDal), Times.Once());
        }

        [TestMethod]
        public void ObtenirSuggestions_DevraitRetournerLesSuggestionsEnOrdreDecroisssantDeScore()
        {
            var resultat = service.ObtenirSuggestionsNomVille(q, latitude, longitude);
            var listeAttendu = resultat.OrderByDescending(r => r.Score);
            listeAttendu.SequenceEqual(resultat).Should().BeTrue();
            
        }
    }
}
