Feature: Obtenir des suggestions de ville

Scenario: Un seul résultat lorsque le nom exact est entré
    When je visite la page de recherche
    And j'entre Amos dans la boite de recherche
    Then je devrais voir un résultat dans la grille        
        | Amos, QC, Canada |

Scenario: Un seul résultat lorsqu'un nom partiel est entré    
    When je visite la page de recherche
    And je suis à la position géographique 43.70011, -79.4163
    And j'entre Londo dans la boite de recherche
    Then je devrais voir les résultats suivants dans la grille       
        | London, ON, Canada   |
        | London, OH, USA      |
        | London, KY, USA      |
        | Londontowne, MD, USA |
